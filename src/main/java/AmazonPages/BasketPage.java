package AmazonPages;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.FluentWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.concurrent.TimeUnit.SECONDS;

public class BasketPage extends Page {

    private static Logger logger = LoggerFactory.getLogger(BasketPage.class);

    @FindBy(xpath = "//span[@id='sc-subtotal-label-activecart']")
    @Title("subTotalItemNum")
    private WebElement subTotalItemNum;

    @FindBy(xpath = "//span[@id='sc-subtotal-amount-activecart']/child::span")
    @Title("totalPrice")
    private WebElement totalPrice;

    @FindBy(xpath = "//div[@class='sc-list-item-content']//following-sibling::span[@class='a-size-medium sc-product-title a-text-bold']")
    @Title("itemTitle")
    private WebElement itemTitle;

    @FindBy(xpath = "//div[@class='sc-list-item-content']//following-sibling::input[@value='Delete']")
    @Title("itemTitle")
    private WebElement deleteItem;

    public BasketPage verifyItemInBasket(String regexp) {
        String title = itemTitle.getText();
        Assert.assertTrue("Wrong item or total: ",
                isMatched(title, regexp)
                && subTotalItemNum.getText().contains("1 item")
                && !totalPrice.getText().contains("0.00"));

        return this;
    }

    public void removeItemFromBasket() {
        click(deleteItem);
        new FluentWait<>(Init.getWebDriver())
                .withTimeout(Init.getTimeOut(), SECONDS)
                .pollingEvery(1, SECONDS)
                .until(driver -> subTotalItemNum.getText().contains("0 items") &&
                                        totalPrice.getText().contains("0.00"));
    }
}
