package AmazonPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HeaderPage extends Page {

    private static Logger LOG = LoggerFactory.getLogger(HeaderPage.class);

    @FindBy(xpath = "//a[@id='nav-cart']")
    @Title("basketIcon")
    protected WebElement basketIcon;

    @FindBy(xpath = "//a[@data-nav-ref='nav_signin']/span[1]")
    @Title("signinButton")
    protected WebElement signinButton;

    @FindBy(xpath = "//a[@id='nav-link-yourAccount']")
    @Title("siginBlock")
    protected WebElement siginBlock;

    @FindBy(xpath = "//a[@data-nav-role='signin']/span[1]")
    @Title("signinMarker")
    protected WebElement signinMarker;

    @FindBy(xpath = "//a[@id='nav-item-signout']")
    @Title("signOut")
    protected WebElement signOutButton;

    @FindBy(xpath = "//input[@id='twotabsearchtextbox']")
    @Title("searchInput")
    protected WebElement searchInput;

    @FindBy(xpath = "//input[@type='submit']")
    @Title("submitButton")
    protected WebElement submitButton;

    @FindBy(xpath = "//select[@id='sort']")
    @Title("sortSelect")
    protected WebElement sortSelect;

    public static class SearchSorting {
        public static String SORT_BY_FEATURED = "relevanceblender";
        public static String SORT_BY_PRICE_LOW_TO_HIGHT = "price-asc-rank";
        public static String SORT_BY_PRICE_HIGHT_TO_LOW = "price-desc-rank";
        public static String SORT_BY_AVG_CUSTOMER_REVIEW = "review-rank";
        public static String SORT_BY_NEWEST_ARRIVALS = "date-desc-rank";
    }

    public void searchItem(String item) {
        type(searchInput, item);
        click(submitButton);
    }

    public void sortItems(String criteria) {
        new Select(sortSelect).selectByValue(criteria);
    }

    public SiginPage moveToSignInPage() {
        Actions action = new Actions(Init.getWebDriver());
        action.moveToElement(siginBlock).build().perform();
        click(signinButton);
        return new SiginPage();
    }

    public boolean verifySignIn(String user) {
        String expectedText = String.format("Hello, %s", user);
        return expectedText.equals(signinMarker.getText());
    }

    public void signOut() {
        Actions action = new Actions(Init.getWebDriver());
        action.moveToElement(siginBlock).build().perform();
        click(signOutButton);
    }

}