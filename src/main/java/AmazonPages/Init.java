package AmazonPages;

import TestData.Amazon;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class Init {

    private static Logger log =  LoggerFactory.getLogger(Init.class);

    private static WebDriver webDriver;
    private static final long timeOut = 5000;
    private static Properties props;
    public static Map<String, Object> stash = new HashMap<>();

    public static String getProperty(String key)  {
        if (null == props) {
            props = new Properties();
            String target = System.getProperty("target", "test");
            try {
                props.load(new FileReader(new File(
                        String.format("src/test/resources/%s.properties", target))));
            } catch (IOException e) {
                log.error("Can't read from property", e);
                Assert.fail(e.getMessage());
            }
        }
        return props.getProperty(key);
    }

    public static WebDriver getWebDriver() {
        if (null == webDriver) {
            File chromeDriver = new File("src/test/resources/drivers/chromedriver");
            System.setProperty("webdriver.chrome.driver", chromeDriver.getAbsolutePath());
            webDriver = new ChromeDriver();
        }
        webDriver.manage().window().maximize();
        return webDriver;
    }

    public static long getTimeOut() {
        return timeOut;
    }

    public static List<Amazon> getDataFromJson(String path) {
        try(BufferedReader br = new BufferedReader(new FileReader(new File(path)))){
            String json = "";
            String line = br.readLine();
            while (line != null) {
                json += line;
                line = br.readLine();
            }
            Gson gson = new Gson();
            Type collection = new TypeToken<List<Amazon>>(){}.getType();
            return gson.fromJson(json, collection);
        } catch (IOException e) {
            log.error("Can't read from file", e);
            Assert.fail(e.getMessage());
        }
        return null;
    }
}
