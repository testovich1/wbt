package AmazonPages;


import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ItemPage extends HeaderPage {

    private static Logger logger = LoggerFactory.getLogger(ItemPage.class);

    @FindBy(xpath = "//input[@id='add-to-cart-button']")
    @Title("addToBasketButton")
    private WebElement addToBasketButton;

    @FindBy(xpath = "//span[@id='attach-sidesheet-view-cart-button']")
    @Title("goToBasket")
    private WebElement goToBasketButton;

    @FindBy(xpath = "//a[@id='hlb-view-cart-announce']")
    @Title("editBasket")
    private WebElement editBasket;

    public void addItemToBasket() {
        click(addToBasketButton);
        initPage();
        try {
            click(goToBasketButton);
        } catch (NoSuchElementException e) {
            logger.info("no side form");
            click(editBasket);
        }
    }
}
