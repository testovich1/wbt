package AmazonPages;

import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Page {

    private static Logger log = LoggerFactory.getLogger(Page.class);
    protected Map<String, WebElement> elementsMap;

    Page() {
        PageFactory.initElements(Init.getWebDriver(), this);
        waitForPageBecomesReady();
        elementsMap = getPageElements();
        log.info(this.getClass().getName());
    }

    protected void initPage() {
        PageFactory.initElements(Init.getWebDriver(), this);
        waitForPageBecomesReady();
    }

    protected void waitForPageBecomesReady() {
        long timeOut = System.currentTimeMillis() + Init.getTimeOut();
        while (timeOut > System.currentTimeMillis()) {
            try {
                if ("complete".equalsIgnoreCase((String)((JavascriptExecutor) Init.getWebDriver())
                        .executeScript("return document.readyState", new Object[0]))) {
                    return;
                }
            } catch (Exception e) {
                log.error("troubles with JavascriptExecutor", e);
                Assert.fail(e.getMessage());
            }
        }
        throw new TimeoutException("Timeout of waiting till page becomes ready");
    }

    protected void click(WebElement element) {
        log.debug("clicking element: " + getElementTitle(element));
        waitForElement(element);
        element.click();
    }

    protected void clickJs(WebElement element) {
        log.debug("clicking element via JS: " + getElementTitle(element));
        JavascriptExecutor executor = (JavascriptExecutor) Init.getWebDriver();
        executor.executeScript("arguments[0].click();", element);
    }

    protected void type(WebElement element, String text) {
        log.debug("clicking element: " + getElementTitle(element));
        waitForElement(element);
        element.click();
        element.clear();
        element.sendKeys(text);
    }

    protected void waitForElement(WebElement element) {
        new WebDriverWait(Init.getWebDriver(), Init.getTimeOut())
                .until(ExpectedConditions.visibilityOf(element));
    }

    private String getElementTitle(WebElement element) {
        Class<?extends Page> clazz = this.getClass();
        String result = "No title for element";
        for(Field field : clazz.getDeclaredFields()) {
            if (field.getType() == WebElement.class && field.isAnnotationPresent(Title.class)) {
                field.setAccessible(true);
                try {
                    if(element == field.get(this)) {
                       Title title = field.getAnnotation(Title.class);
                       result =  (null != title) ? title.value() : "";
                       break;
                    }
                } catch (IllegalAccessException e) {
                    log.debug("Can't get annotation of element");
                }
            }
        }
        return result;
    }

    protected Map<String, WebElement> getPageElements() {
        Map<String, WebElement> elements = new HashMap<>();
        Class<? extends Page> clazz = this.getClass();
        for(Field field : clazz.getDeclaredFields()) {
            if (field.getType() == WebElement.class && field.isAnnotationPresent(Title.class)) {
                field.setAccessible(true);
                try {
                    elements.put(field.getAnnotation(Title.class).value(), (WebElement) field.get(this));
                } catch (IllegalAccessException e) {
                    log.debug("Can't get annotation of element");
                }
            }
        }
        return elements;
    }

    protected boolean isMatched(String source, String regex) {
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(source);
        return m.matches();
    }

}