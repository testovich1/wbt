package AmazonPages;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.FluentWait;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.concurrent.TimeUnit.SECONDS;

public class SearchResultPage extends HeaderPage {

    @FindBy(xpath = "//li/div[@class='s-item-container']/descendant::h2/..")
    @Title("sortSelect")
    protected List<WebElement> searchResultList;

    @FindBy(xpath = "//span[@id='s-result-count']")
    @Title("searchResultNum")
    protected WebElement searchResultNum;

    public ItemPage selectItem(String regexp, Integer itemNumber) {
        List<WebElement> sortedItems = getMatchItemByCriteria(regexp);
        Assert.assertTrue(itemNumber <= sortedItems.size());

        click(sortedItems.get(itemNumber - 1));
        return new ItemPage();
    }

    private List<WebElement> getMatchItemByCriteria(String regexp) {
        new FluentWait<>(Init.getWebDriver())
                .withTimeout(Init.getTimeOut(), SECONDS)
                .pollingEvery(1, SECONDS)
                .until(driver -> searchResultNum.isDisplayed());

        return searchResultList.stream()
                .filter(i->isMatched(i.getText(), regexp))
                .collect(Collectors.toList());
    }

}
