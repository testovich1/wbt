package AmazonPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SiginPage extends Page {

    private static Logger logger = LoggerFactory.getLogger(SiginPage.class);

    @FindBy(xpath = "//input[@id='ap_email']")
    @Title("emailInput")
    private WebElement emailInput;

    @FindBy(xpath = "//input[@id='ap_password']")
    @Title("passwordInput")
    private WebElement passwordInput;

    @FindBy(xpath = "//input[@id='signInSubmit']")
    @Title("signInSubmit")
    private WebElement signInSubmit;

    public HeaderPage signIn(String login, String password) {
        type(emailInput, login);
        type(passwordInput, password);
        click(signInSubmit);
        return new HeaderPage();
    }

}
