package StepDefs;

import AmazonPages.*;
import TestData.Amazon;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.util.List;
import java.util.stream.Collectors;

import static AmazonPages.HeaderPage.SearchSorting.SORT_BY_PRICE_LOW_TO_HIGHT;

public class AmazonStepDefs {

    private Amazon testObj = new Amazon();

    @Before
    public void initData() {
        Init.getWebDriver();
    }

    @After
    public void eraseData() {
        Init.getWebDriver().quit();
    }

    @Given("^User get test data for \"([^\"]*)\"$")
    public void getTestData(String fileName) {
       final String dataPath = Init.getProperty("test_data." + fileName);
       List<Amazon> data = Init.getDataFromJson(dataPath);
       Init.stash.put("amazon", data);
    }

    @SuppressWarnings("unchecked")
    private Amazon getSuite(String suite) {
        return ((List<Amazon>)  Init.stash.get("amazon")).stream()
                .filter(i->i.getSuite().equals(suite)).collect(Collectors.toList()).get(0);
    }

    @When("^User choose data from suite \"([^\"]*)\"$")
    public void getTestDataBySuite(String suite) {
        testObj = getSuite(suite);
    }

    @And("^User open main page and sign in$")
    public void signInToAmazon() {
        Init.getWebDriver().get(testObj.getUrl());
        Assert.assertTrue("Can not sign in",
            new HeaderPage()
                .moveToSignInPage()
                .signIn(testObj.getLogin(), testObj.getPassword())
                .verifySignIn(testObj.getUser()));
    }

    @Then("^User search item$")
    public void searchItem() {
        new HeaderPage().searchItem(testObj.getItem());
    }

    @And("^User sorting result by lowest price$")
    public void sortingSearchResults() {
        new HeaderPage().sortItems(SORT_BY_PRICE_LOW_TO_HIGHT);
    }

    @When("^User select sorted item$")
    public void selectSortedItem() {
        new SearchResultPage().selectItem(testObj.getReqxp(), 1);
    }

    @Then("^User open Item page and put in basket$")
    public void putItemInBasket() {
        new ItemPage().addItemToBasket();
    }

    @And("^User open BasketPage verify item and remove it$")
    public void userVerifyAndRemoveItemFromBasket(){
        new BasketPage()
                .verifyItemInBasket(testObj.getReqxp())
                .removeItemFromBasket();
    }

    @Then("^User sign out$")
    public void userSignOut() {
        new HeaderPage().signOut();
    }

}
