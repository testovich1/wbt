package TestData;

import com.google.gson.annotations.Expose;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class Amazon {

    @Expose
    private String suite;
    @Expose
    private String item;
    @Expose
    private String reqxp;
    @Expose
    private String url;
    @Expose
    private String login;
    @Expose
    private String user;
    @Expose
    private String password;
}
