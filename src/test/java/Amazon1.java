import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(monochrome = true, format = {"pretty"},
        glue = {"StepDefs"},
        features = {"src/test/resources/features"},tags = {"@test01"},
        snippets = SnippetType.CAMELCASE)
public class Amazon1 {
}
