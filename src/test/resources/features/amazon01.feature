
   Feature: Amazon surf and turf test

     @test01
     Scenario: User open Amazon page, login, search item by smallest price, put it into basket and verify it, remove and logout
       Given User get test data for "amazon"
       When User choose data from suite "1"
       And User open main page and sign in
       Then User search item
       And User sorting result by lowest price
       When User select sorted item
       Then User open Item page and put in basket
       And User open BasketPage verify item and remove it
       Then User sign out